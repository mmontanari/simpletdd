#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <setjmp.h>
#include "cmocka.h"

static void collatz(const int n, int *sequence){
    int i = 0;

    if (n <= 0){
        sequence[0] = 0;
        return;
    }

    sequence[i] = n;

    while ( sequence[i] != 1){
        i++;
        if(sequence[i-1] % 2 == 0){
            sequence[i] = sequence[i-1]/2;
        }
        else {
            sequence[i] = 3 * sequence[i-1] + 1;
        }
    }
}

static void even_test(void **state){
    int *sequence = *state;
    collatz(4, sequence);
    int i = 0;

    assert_int_equal(4,sequence[i++]);
    assert_int_equal(2,sequence[i++]);
    assert_int_equal(1,sequence[i++]);
}

static void odd_test(void **state){
    int *sequence = *state;
    collatz(3, sequence);
    int i = 0;

    assert_int_equal(3,sequence[i++]);
    assert_int_equal(10,sequence[i++]);
    assert_int_equal(5,sequence[i++]);
    assert_int_equal(16,sequence[i++]);
    assert_int_equal(8,sequence[i++]);
    assert_int_equal(4,sequence[i++]);
    assert_int_equal(2,sequence[i++]);
    assert_int_equal(1,sequence[i++]);
}

static void simple_test(void **state){
    int sequence[1];
    collatz(1, sequence);
    assert_int_equal(1,sequence[0]);

    collatz(-1, sequence);
    assert_int_equal(0,sequence[0]);
}

static void no_test(void **state){
    int *sequence = *state;
    collatz(11, sequence);
}

static int setup(void **state) {
    int *sequence = calloc(5, sizeof(sequence));
    if (sequence == NULL ){
        return -1;
    }
    *state= sequence;
    return 0;
}

static int cleanup(void **state) {
    free(*state);
    return 0;
}

int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(simple_test),
            cmocka_unit_test_setup_teardown(even_test, setup, cleanup),
            cmocka_unit_test_setup_teardown(odd_test, setup, cleanup),
            cmocka_unit_test_setup_teardown(no_test, setup, cleanup),
    };
    return cmocka_run_group_tests_name("success_test" ,tests, NULL, NULL);
}
